<?php

require_once __DIR__ . '/../../../inc/includes.php';

class TestClass {

   static function getTicketsCount() {
      global $DB;
      $q = "SELECT count(*) as `count` FROM `glpi_tickets`";
      if ($result = $DB->query($q)) {
         if($data = $result->fetch_row()) {
            return $data[0];
         }
      }
      return false;
   }

   static function getTicketsCountCache() {
      $cache = new PluginCacheAPCu('TestClass::getTicketsCount');
      if ($cache->exists()) {
         $result = $cache->fetch();
      } else {
         $result = self::getTicketsCount();
         $cache->store($result);
      }
      return $result;

   }

}

PluginCacheAPCu::clear();

echo 'Execute query<br />';
$start = microtime(true);
TestClass::getTicketsCount();
echo (microtime(true) - $start) . '<br />'; 

echo 'Execute query and store result to cache<br />';
$start = microtime(true);
TestClass::getTicketsCountCache();
echo (microtime(true) - $start) . '<br /><br />'; 

PluginCacheAPCu::clear();

echo 'Execute 100 queries<br />';
$start = microtime(true);
for ($i = 0; $i<= 100; $i++) {
   TestClass::getTicketsCount();
}
echo (microtime(true) - $start) . '<br />'; 

echo 'Execute 100 queries with stored cache<br />';
$start = microtime(true);
for ($i = 0; $i<= 100; $i++) {
   TestClass::getTicketsCountCache();
}
echo (microtime(true) - $start) . '<br /><br />';

echo 'Execute query<br />';
$start = microtime(true);
TestClass::getTicketsCount();
echo (microtime(true) - $start) . '<br />'; 

echo 'Execute query whit stored cache<br />';
$start = microtime(true);
TestClass::getTicketsCountCache();
echo (microtime(true) - $start) . '<br />';
