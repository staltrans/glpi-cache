<?php
/*
 -------------------------------------------------------------------------
 Cache plugin for GLPI
 Copyright (C) 2017 by the Cache Development Team.

 https://bitbucket.org/staltrans/cache
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Cache.

 Cache is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Cache is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Cache. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

/**
 * Plugin install process
 *
 * @return boolean
 */
function plugin_cache_install() {
   return true;
}

/**
 * Plugin uninstall process
 *
 * @return boolean
 */
function plugin_cache_uninstall() {
   PluginCacheAPCu::clear();
   return true;
}
