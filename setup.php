<?php
/*
 -------------------------------------------------------------------------
 Cache plugin for GLPI
 Copyright (C) 2017 by the Cache Development Team.

 https://bitbucket.org/staltrans/cache
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Cache.

 Cache is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Cache is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Cache. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

define('PLUGIN_CACHE_VERSION', '0.0.1');

/**
 * Init hooks of the plugin.
 * REQUIRED
 *
 * @return void
 */
function plugin_init_cache() {
   global $PLUGIN_HOOKS;

   $PLUGIN_HOOKS['csrf_compliant']['cache'] = true;
}


/**
 * Get the name and the version of the plugin
 * REQUIRED
 *
 * @return array
 */
function plugin_version_cache() {
   return [
      'name'           => 'Cache',
      'version'        => PLUGIN_CACHE_VERSION,
      'author'         => '<a href="https://bitbucket.org/staltrans/">StalTrans</a>',
      'license'        => 'GPLv3',
      'homepage'       => 'https://bitbucket.org/staltrans/glpi-cache',
      'minGlpiVersion' => '9.1'
   ];
}

/**
 * Check pre-requisites before install
 * OPTIONNAL, but recommanded
 *
 * @return boolean
 */
function plugin_cache_check_prerequisites() {
   // Strict version check (could be less strict, or could allow various version)
   if (!function_exists('apcu_fetch') && !ini_get('apc.enable')) {
      printf(__('Requires %s', 'cache'), '<a href="https://pecl.php.net/APCu">APC User Cache</a>');
   }
   if (version_compare(GLPI_VERSION, '9.1', 'lt')) {
      if (method_exists('Plugin', 'messageIncompatible')) {
         echo Plugin::messageIncompatible('core', '9.1');
      } else {
         printf(__('This plugin requires GLPI >= %s', 'cache'), '9.1');
      }
      return false;
   }
   return true;
}

/**
 * Check configuration process
 *
 * @param boolean $verbose Whether to display message on failure. Defaults to false
 *
 * @return boolean
 */
function plugin_cache_check_config($verbose = false) {
   if (true) { // Your configuration check
      return true;
   }

   if ($verbose) {
      _e('Installed / not configured', 'cache');
   }
   return false;
}
