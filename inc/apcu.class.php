<?php

/**
 -------------------------------------------------------------------------
 Cache plugin for GLPI
 Copyright (C) 2017 by the Cache Development Team.

 https://bitbucket.org/staltrans/cache
 -------------------------------------------------------------------------

 LICENSE

 This file is part of Cache.

 Cache is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 Cache is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with Cache. If not, see <http://www.gnu.org/licenses/>.
 --------------------------------------------------------------------------
 */

class PluginCacheAPCu {

   // Store the variable using this name.
   protected $key;

   /**
    * @param string $callable Function or method name
    * @param mixed $variaboes Variables passed to a function or method $callable
    *
    * @return void
    */
   function __construct($callable, $variables = null) {
      $this->key = $callable;
      if (isset($variables)) {
         $this->key .= '-';
         $this->key .= md5(json_encode($variables));
      }
   }

   /**
    *  Cache a variable in the data store
    *
    * @param mixed $result The variable to store
    * @param int $ttl Time to live the cache
    *
    * @return bool
    */
   function store($result, $ttl = null) {
      return apcu_store($this->key, $result, $ttl);
   }

   /**
    * Fetch a stored variable from the cache
    *
    * @return mixed
    */
   function fetch() {
      return apcu_fetch($this->key);
   }

   /**
    * Checks if entry exists
    *
    * @return bool
    */
   function exists() {
      return apcu_exists($this->key);
   }

   /**
    * Removes a stored variable from the cache
    *
    * @return bool
    */
   function delete() {
      return apcu_delete($this->key);
   }

   /**
    * Clears the APCu cache
    *
    * @return bool
    */
   static function clear() {
      return apcu_clear_cache();
   }

   /**
    * Retrieves cached information from APCu's data store
    *
    * @return array
    */
   static function info() {
      return apcu_cache_info();
   }

}
