<?php

require_once __DIR__ . '/../../../inc/includes.php';

class TestClass {

   static function plus($a, $b) {
      $cache = new PluginCacheAPCu('TestClass::plus', array($a, $b));
      if ($cache->exists()) {
         echo "Fetch variable from cache:" . PHP_EOL;
         $result = $cache->fetch();
         var_dump($result);
      } else {
         $result = $a + $b;
         echo "Store $result to cache" . PHP_EOL;
         $cache->store($result);
      }
      echo '<pre>' . print_r($cache->info(), true) . '</pre>';
      return $result;

   }

}

echo TestClass::plus(2, 5);
echo PHP_EOL;
echo TestClass::plus(5, 2);
echo PHP_EOL;
